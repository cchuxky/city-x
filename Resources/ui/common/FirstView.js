//FirstView Component Constructor
function FirstView() {
	//create object instance, a parasitic subclass of Observable
	var self = Ti.UI.createView();
	
	var logoLabel = Titanium.UI.createLabel({
		text: 'Find Friend',
		top: '30dp',
		font: {fontSize: '30dp'},
	});
	self.add(logoLabel);



	return self;
}

module.exports = FirstView;
