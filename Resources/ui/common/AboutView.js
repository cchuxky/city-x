function About(){
	
	var window = Ti.UI.createWindow({
		height: '100%',
		width: '100%',
		bottom: Ti.Platform.displayCaps.platformHeight,
		fullscreen: true,
	});
	
	var header = Ti.UI.createLabel({
		width: '100%',
		height: '60dp',
		text: 'ABOUT THIS APP',
		textAlign: 'center',
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: '#000000',
		top: 0,
	});
	//window.add(header);
	/*var loadingInd = Titanium.UI.createActivityIndicator({
			width: '50dp',
			height: '50dp',
			left: 200,
		});
	*/
	var view = Ti.UI.createWebView({
		top: 0,
		url: '',
		disableBounce: true,
		width: '100%',
		height: '100%',
		loading: false,
		enableZoomControls: false,
	});
	window.add(view);
	/*window.add(loadingInd);
	loadingInd.show();
	view.addEventListener('load', function(){
		loadingInd.hide();
	});
	*/
	var backBtn = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: 'BACK',
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: Titanium.App.Properties.getString('buttonBg3'),
		bottom: 0,
	});
	
	window.add(backBtn);
	var aUp = Ti.UI.createAnimation({
		bottom: Ti.Platform.displayCaps.platformHeight,
		duration: 500,
	});
	backBtn.addEventListener('click', function(){
		window.animate(aUp);
		aUp.addEventListener('complete', function(){
			Ti.API.info('animation complete');
			window.close();
			
		});
		
	});
	
	
	return window;

};

module.exports = About;
