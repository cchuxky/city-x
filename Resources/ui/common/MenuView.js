function Menu(win){
	
	var shell = Ti.UI.createView({
		height: '100%',
		backgroundColor: Ti.App.Properties.getString('backgroundColor'),
		bottom: '-1000dp',
	});
	
	var view = Ti.UI.createScrollView({
		layout: 'vertical',
		//backgroundColor: '#ccccccc',
		//height: '300dp',
		top: 0,
	});
	
	shell.add(view);
	
	var status = Ti.UI.createActivityIndicator({
		style: Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN,
		left: 50,
	});
	status.hide();
	
	var session = Ti.App.Properties.getObject('user');

	
	var nameBtn = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: session.username.toUpperCase(),
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color: '#CCCCCC',
		backgroundColor: '#ffffff',
	});
	
	
	view.add(nameBtn);
	
	var inviteBtn = Ti.UI.createView({
		width: '100%',
		height: '60dp',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: Titanium.App.Properties.getString('buttonBg1'),
		action: 'invite1',
	});
	
	var inviteText = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: 'INVITE',
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: Titanium.App.Properties.getString('buttonBg1'),
		action: 'invite',
	});
	
	inviteBtn.add(inviteText);
	
	view.add(inviteBtn);
	var twitterbook = require('es.appio.twitterbook');
	
	
	var feebackOptions = function() { // create the actions view - the one will be revealed on swipe
	
		var view = Ti.UI.createView({
			height:Ti.UI.SIZE,
			width:322,
			left: 320,
			layout: 'horizontal',
			bubbleParent: false,
		});
		
		var b1 = Ti.UI.createButton({
			width: '50%',
			height: '60dp',
			left:0,
			title: 'EMAIL',
			font: {fontSize: '20dp', fontFamily: 'NexaBlack'},
			style: 'none',
			color:Titanium.App.Properties.getString('buttonColor'),
			backgroundColor: '#6666CC',
			action: 'email',
		});
		
		
		var b2 = Ti.UI.createButton({
			width: '49%',
			height: '60dp',
			title: 'RATE',
			font: {fontSize: '20dp', fontFamily: 'NexaBlack'},
			style: 'none',
			color:Titanium.App.Properties.getString('buttonColor'),
			backgroundColor: '#996699',
			action: 'rate',
		});
		
		view.add(b1);
		view.add(b2);
		
		b1.addEventListener('click', function(){
			share('email');
		});
		
		b2.addEventListener('click', function(){
			share('rate');
		});
		
		var share = function(e){	
			Ti.API.info(e);	
		 	if (e == 'email'){
		 		var emailDialog = Ti.UI.createEmailDialog()
				emailDialog.subject = "Feedback from "+session.username;
				emailDialog.toRecipients = ['thachc@gmail.com'];
				emailDialog.messageBody = 'Oi!';
				//var f = Ti.Filesystem.getFile('cricket.wav');
				//emailDialog.addAttachment(f);
				emailDialog.open();
				
		 	}
		 	
		 	if (e == 'rate'){
		 		Ti.Platform.openURL(Ti.App.Properties.getString('appstoreurl'));
		 	}
		 	
	 		var aDown = Ti.UI.createAnimation({
				bottom: '-600dp',
				duration: 500,
			});
			Ti.App.fireEvent('menuToggled', function(){});
			shell.animate(aDown);
		}
	
	
		return view;
	};
	
	
	
	
	var feedbackBtn = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: 'FEEDBACK',
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: Titanium.App.Properties.getString('buttonBg2'),
		action: 'feedback',
	});
	
	view.add(feedbackBtn);
	
	feedbackBtn.addEventListener('singletap', function(){
		var v1 = feebackOptions();
		feedbackBtn.add(v1);
		v1.animate({
			left: 0,
			duration: 200,
		});
		
		
	});
	
	
	var aboutBtn = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: 'ABOUT THIS APP?',
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: Titanium.App.Properties.getString('buttonBg3'),
	});
	
	view.add(aboutBtn);
	
	var aDown = Ti.UI.createAnimation({
		bottom: 0,
		duration: 500,
	});
	
	aboutBtn.addEventListener('click', function(){
		//Ti.App.fireEvent('closeMenu');
		//shell.animate(aDown);
		var About = require('ui/common/AboutView');
		var aboutView = new About();
		aboutView.open();
		aboutView.animate(aDown);
		
	});
	
	var settingsBtn = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: 'SETTINGS',
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: Titanium.App.Properties.getString('buttonBg4'),
	});
	
	view.add(settingsBtn);
	
	
	var logoutBtn = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: 'LOGOUT',
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: '#e4491c',
	});
	
	view.add(logoutBtn);
	
	logoutBtn.addEventListener('click', function(){
		
		Ti.App.fireEvent('logout', function(){});
	});
	

	
	return shell;

};

module.exports = Menu;
