exports.Main = function(){
	var session = Ti.App.Properties.getObject('user'); 
	var imagePath = Ti.App.Properties.getString('imagePath');
	var win = Ti.UI.createWindow({
		navBarHidden: true,
		backgroundColor: '#000',//Ti.App.Properties.getString('backgroundColor'),
		//barColor: Ti.App.Properties.getString('barColor'),
		//navTintColor: Ti.App.Properties.getString('tintColor'),
		fullscreen: true,
	});
	
	var blockedByMe;
	var blockedMe;
	
	
	var self = Ti.UI.createView({
		backgroundColor: Ti.App.Properties.getString('backgroundColor'),
		barColor: Ti.App.Properties.getString('barColor'),
		layout: 'vertical',
	});
	
	var view = Ti.UI.createView({
		top: 0,
		height: '60dp',
	});
	
	var cameraBtn = Ti.UI.createLabel({
		left: 0,
		text: 'REPORT AN ISSUE',
		width: '260dp',
		height: '60dp',
		backgroundColor: '#024F79',
	    style: 'none',
	    textAlign: 'center',
	    color: '#ffffff',
	    font: {fontSize: '20dp', fontFamily: 'NexaBlack'},
	});

	view.add(cameraBtn);
	
	var nav = Ti.UI.createButton({
		width: '60dp',
		height: '60dp',
		top: 0,
		right: 0,
		//borderRadius: '50dp',
		zIndex: 100,
		image: '/images/more-list-7.png',
		backgroundColor: '#00AB2D',
		style: 'none',
	});
	
	win.add(nav);
	
	
	
	self.add(view);
	
	var navView = Ti.UI.createView({
		height: '60dp',
	});
	self.add(navView);
	
	var mineBtn = Ti.UI.createButton({
		width: '50%',
		height: '60dp',
		title: 'MY REPORTS',
		right: 0,
		font: {fontSize: '20dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: '#a8dba8',
	});
	
	navView.add(mineBtn);
	
	var nearbyBtn = Ti.UI.createButton({
		width: '50%',
		height: '60dp',
		title: 'NEARBY',
		left: 0,
		font: {fontSize: '20dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: '#79bd9a',
	});
	
	navView.add(nearbyBtn);
	
	var selector = Ti.UI.createView({
		height: '3dp',
		width: '50%',
		left: 0,
		bottom: 0,
		backgroundColor: '#FF00FF',
	});
	navView.add(selector);
	
	var style = Ti.UI.iPhone.ActivityIndicatorStyle.DARK;
	if (Titanium.Platform.name == 'iPhone OS'){
		style = Ti.UI.iPhone.ActivityIndicatorStyle.DARK;
	}
	var statusInd = Titanium.UI.createActivityIndicator({
		style: style,
	});
	
	
	
	mineBtn.addEventListener('click', function(){
		showNearby(false, false, true);
		var width = Titanium.Platform.displayCaps.platformWidth;
		selector.left = width /2;
	});
	
	nearbyBtn.addEventListener('click', function(){
		Ti.Geolocation.getCurrentPosition(function(e) {
			showNearby(e.coords.latitude, e.coords.longitude);
		});
		selector.left = 0;
	});
	

	
	cameraBtn.addEventListener('click', function(){
		Ti.Geolocation.getCurrentPosition(function(e) {
			
			var cameraTransform = Ti.UI.create2DMatrix();
			cameraTransform = cameraTransform.scale(1);
			Titanium.Media.showCamera({
				success:function(event)
				{
					
					/*var ImageFactory = require('ti.imagefactory');
					var convert = require('lib/convert');
					newImage = convert.resizeKeepAspectRatioNewWidth(event.media, 640);
					newBlob = ImageFactory.compress(newImage, 0.50);
					*/
					
					Ti.Media.hideCamera();
					var Post = require('/ui/common/PostWindow').Post;
					var postWin = new Post(event.media);
					postWin.open();
					
				},
				cancel:function()
				{
					//alert('cancelled');
				},
				error:function(error)
				{
					var a = Titanium.UI.createAlertDialog({title:'Camera'});
					if (error.code == Titanium.Media.NO_CAMERA)
					{
						a.setMessage('Please run this test on device');
					}
					else
					{
						a.setMessage('Unexpected error: ' + error.code);
					}
					a.show();
				},
				transform:cameraTransform,
				showControls:true,	// don't show system controls
				mediaTypes:Ti.Media.MEDIA_TYPE_PHOTO,
				animated: false,
				//overlay: overlay,
				autohide:false, // tell the system not to auto-hide and we'll do it ourself
			});
		});
	});
	
	

	
	if (Ti.Geolocation.locationServicesEnabled) {
		Titanium.Geolocation.purpose = 'For check-ins and search for people nearby.';
		Titanium.Geolocation.ACCURACY_NEAREST_TEN_METERS;
		//Titanium.Geolocation.distanceFilter = 10;
		//Titanium.Geolocation.trackSignificantLocationChange = true;
	} else {
		alert('Please enable location service to use icy');
	}
	
	function getLocation(){
	//Get the current position and set it to the mapview
		Titanium.Geolocation.getCurrentPosition(function(e){	        
		        if (e.success){
		        	var coords = Ti.App.Properties.getObject('coords');
		        	Ti.App.Properties.setObject('coords', {lat: e.coords.latitude, lon: e.coords.longitude});
		        	if (typeof coords === 'object'){
			        	if (coords.lat != e.coords.latitude || coords.lon !== e.coords.longitude){						
							Cloud.Users.update({
								custom_fields: {
									coordinates: [e.coords.longitude, e.coords.latitude]
								}
							}, function(et){
								
							});
			       		}
					}
				} else {
					alert(e.error);
				}
		});
	}
	
	Ti.Geolocation.getCurrentPosition(function(e) {
		showNearby(e.coords.latitude, e.coords.longitude);
	});
	
	Titanium.Geolocation.addEventListener('location', getLocation);
	
	var table = Ti.UI.createTableView({
		//backgroundColor: Ti.App.Properties.getString('backgroundColor'),
		//backgroundImage: '/images/assets/rundlelantern_4854.s800x800.jpg',
		width: '100%',
		top: 0,
		zIndex: 10,
		separatorInsets: {left: 0, right: 0},
		rowHeight: '60dp',
		//style: Ti.UI.iPhone.TableViewStyle.GROUPED,
	});
	//table.hide();
	self.add(table);
	win.add(statusInd);
	
	/*var status = Titanium.UI.createActivityIndicator({
		style: style,
	});
	status.hide();
	self.add(status);
	*/
	var tableHeader = Ti.UI.createView({
		width:'100%',
		height:'60dp',
		//backgroundColor: '#FFFF00',
		color: '#c0c0c0',
	});
	
	var actInd = Ti.UI.createActivityIndicator({
	  color: 'green',
	  font: {fontFamily:'Helvetica Neue', fontSize:26, fontWeight:'bold'},
	  message: 'Loading...',
	  top:10,
	  left:10,
	  height:Ti.UI.SIZE,
	  width:Ti.UI.SIZE
	});
	actInd.show();
	tableHeader.add(actInd);
	
	
	table.headerPullView = tableHeader;
	

	
	function showNearby(lat, lon, me){
		statusInd.show();
		
		var tableData = [];
		//var timeAgo = moment().endOf('minute').startOf('hour').subtract('hours', 0.5).format();
		var q;
		
		if(me){
			q = {user_id: session.id};
		} else {
			q = '';//{"lnglat":{"$nearSphere":[lon,lat], "$maxDistance" : 0.00126}};
		}
		var contentData=[];
		Cloud.Places.query({
			where: q,
			page: 1,
		    per_page: 100,
		    order: '-updated_at'
		}, function (e) {
		    if (e.success) {
		    	
		    	var gotData = false;
		    	
		        for (var i = 0; i < e.places.length; i++) {
		            var place = e.places[i];
		            var row = dataRow(place, true);
		            contentData.push(row);
		         }
		         
		         table.setData([]);
		    	 table.setData(tableData);
				 
		         
		         table.appendRow(contentData);
		         table.show();
		    } else {
		        alert('Error:\n' +
		            ((e.error && e.message) || JSON.stringify(e)));
		    }
		    statusInd.hide();
		});
	}
	
	
	
	function dataRow(data, addFriend){
		if (Titanium.Platform.name == 'iPhone OS'){
			var selectionStyle = Titanium.UI.iPhone.TableViewCellSelectionStyle.NONE;
		} else {
			var selectionStyle = Titanium.UI.TableViewCellSelectionStyle.NONE;
		}
		var row = Ti.UI.createTableViewRow({
			height: Ti.UI.SIZE,
			backgroundColor:'#fff',
			action: data,
			selectionStyle: selectionStyle,
			
		});
		
		row.place = data;
		
		var outlineView = Ti.UI.createView({
			top: 0,
			width: '100%',
			layout: 'vertical',
			height: Ti.UI.SIZE,
			backgroundColor: '#fff',
		});

		var convert = require('lib/convert');
/*		var image =  convert.Utils.RemoteImage({
			//defaultImage: '/images/placeholder-100x125.png',
			image:data.photo.urls.original, 
			height: 'auto',
			width: 'auto',
		});
*/

	    var image = Titanium.UI.createImageView({
	    	image: data.photo.urls.medium_640,
	    	width: '320dp',
	    });
	    
	    var imageBox = Ti.UI.createView({
	    	width: '320dp',
	    	height: '320dp',
	    	backgroundImage: image.toImage(),
	    	backgroundSize: 'cover'
	    });
	    //imageBox.add(image);
	    
/*	    var imageResized = ImageFactory.imageAsThumbnail(image.toBlob(), {size:320, quality:ImageFactory.QUALITY_BEST });
		if (imageResized){
			var imageCropped = ImageFactory.imageAsCropped(imageResized, { width:320, height:320});
		} else {
			imageCropped = image.toBlob();
		}
	    
	    var imageBox = Titanium.UI.createImageView({
	    	image: imageResized,
	    	width: '320dp',
	    	height: '320dp',
	    });
*/	    
/*		var imageBox = Titanium.UI.createImageView({
	    	width: '320dp',
	    	height: '320dp',
	    });
		convert.getRemoteFile(data.photo.urls.medium_640, function(ev){
			
			Ti.API.info('get filed---------');
			Ti.API.info(ev);
        	var image = Ti.UI.createImageView({
        		image: ev,
        		width: 'auto',
        		height: 'auto',
        	});
        	
        	var imageResized = ImageFactory.imageAsThumbnail(image.toBlob(), {size:320, quality:ImageFactory.QUALITY_BEST });
        	imageBox.image = imageResized;
        });

*/
	    //var localCreated = moment.utc(data.created_at);
	    var headView = Ti.UI.createView({
			top: 10,
			left: 10,
			width: '100%',
			//backgroundColor: '#ffffcc',
			height: '30dp',
			//id: data.user.id,
		});
	    var localCreated = moment.utc(data.created_at);
	    var createdLabel = Ti.UI.createLabel({
	    	text: moment(localCreated, "YYYYMMDD").fromNow(),
	    	right: 20,
	    	bottom: 4,
	    	disableBounce: true,
	    	font: {fontSize: '13dp'},
			color: '#ccc',
	    });
	    headView.add(createdLabel);
	       
	    
	    var userName = Ti.UI.createLabel({
	    	text: data.user.username,
	    	left: 10,
	    	bottom: 4,
	    	disableBounce: true,
	    	font: {fontWeight: 'bold', fontSize: '14dp'},
	    	color: '#3E474E',
	    	action: {name: 'user', user_id: data.user.id},
	    });
	    headView.add(userName);
	    
	    var desc = Ti.UI.createLabel({
	    	text: data.name,
	    	left: 15,
	    	top: 10,
	    	disableBounce: true,
	    	font: {fontSize: '16dp'},
			color: '#003C5B',
	    });

		var buttonView = Ti.UI.createView({
			width: '100%',
			height: 50,
			left: 10,
			//bottom: 0,
		});
		
		
		var likeBtn = Ti.UI.createButton({
			style: 'none',
			borderWidth: 1,
			borderColor: Ti.App.Properties.getString('buttonBg1'),
			width: 80,
			height: 30,
			title: 'VOTE',
			left: 0,
			color: Ti.App.Properties.getString('buttonTextColor'),
			font: {fontSize: '12dp', fontWeight: 'bold'},
			textAlign:'left',
			action: {name: 'like', user_id: data.user.id},
		});
		
		buttonView.add(likeBtn);
		
		var likeCount = Ti.UI.createLabel({
			text: data.likes_count ? data.likes_count : 0,
			height: 30,
			width: 30,
			backgroundColor: Ti.App.Properties.getString('buttonBg1'),
			right: 0,
			font: {fontSize: '12dp'},
			textAlign: 'center',
			color: '#fff',
		});
		
		likeBtn.add(likeCount);
		
		var chatBtn = Ti.UI.createButton({
			style: 'none',
			borderWidth: 1,
			borderColor: Ti.App.Properties.getString('buttonBg1'),
			height: 30,
			title: 'COMMENT',
			left: 85,
			width: 80,
			color: Ti.App.Properties.getString('buttonTextColor'),
			font: {fontSize: '12dp', fontWeight: 'bold'},
			textAlign: 'center',
		});
		
		buttonView.add(chatBtn);
		
		
		
		outlineView.add(headView);
		outlineView.add(imageBox);
		outlineView.add(desc);
		outlineView.add(buttonView);
		row.add(outlineView);
		
	    return row;
	}
	
	
	var pulling = false;
	var reloading = false;
	
	function beginReloading()
	{
		// just mock out the reload
		setTimeout(endReloading,2000);
	}
	var lastRow = 4;
	
	function endReloading()
	{
		Ti.Geolocation.getCurrentPosition(function(e) {
			showNearby(e.coords.latitude, e.coords.longitude);
		});
		getLocation();
		// when you're done, just reset
		table.setContentInsets({top:0},{animated:true});
		reloading = false;
		actInd.hide();
	}

	
	
	table.addEventListener('scroll',function(e)
	{
		var offset = e.contentOffset.y;
		if (offset <= -65.0 && !pulling && !reloading)
		{
			pulling = true;

		}
		else if (pulling && (offset > -65.0 && offset < 0) && !reloading )
		{
			pulling = false;
		} 
		else if (offset >= 135) 
		{
			//navBar.show();
		} else {
			//navBar.hide();
		}
	});
	
	var event1 = 'dragEnd';
	if (Ti.version >= '3.0.0') {
		event1 = 'dragend';
	}

	table.addEventListener(event1,function(e)
	{
		if (pulling && !reloading)
		{
			reloading = true;
			pulling = false;
			actInd.show();
			table.setContentInsets({top:60},{animated:true});
			beginReloading();
		}
	});

	table.addEventListener('click', function(e){
		row = e.rowData;
		var action = e.source.action;
		
		if (action) {
			if (action.name == 'user'){
				//var Profile = require('/ui/common/UserProfileWindow').Profile;
				//var	profileWin = new Profile(action.user_id);
				//window.containingTab.open(profileWin);
			} else if (action.name == 'like'){
				Ti.API.info('lieddddddddddddd');
				var children = e.source.children;
		
				if (children) {
			        for (var c = children.length - 1; c >= 0; c--) {
			            var child = children[c];
			        }
			    }

				
				Cloud.Likes.create({
				    place_id: row.place.id,
				}, function (e) {
				    if (e.success) {
				        var like = e.likes[0];
				        child.text = child.text+1;
				    } else {
				    	
				    	Cloud.Likes.remove({
						    place_id: row.place.id
						}, function (er) {
						    if (er.success) {
						        //alert('Success');
						        child.text = child.text - 1;
						    } else {
						        //alert('Error:\n' +
						        //    ((e.error && e.message) || JSON.stringify(e)));
						    }
						});
				    }
				});
			}
		} else if(row.comment){
			//var Chat = require('/ui/common/ChatWindow').Chat;
			//var	chatWin = new Chat(row.checkin);
			
		}
	});
	
	win.add(self);
	

	
	var menuToggled = false;
	
	var Menu = require('ui/common/MenuView');
		
	var menuView = new Menu(win);
	win.add(menuView);
	
	var aUp = Ti.UI.createAnimation({
		bottom: 0,
		duration: 500,
	});
	
	var aDown = Ti.UI.createAnimation({
		bottom: '-600dp',
		duration: 500,
	});
	
	
	menuView.addEventListener('click', function(e){
		var action = e.source.action;
		//Ti.API.info(action);
		if (action !== 'feedback'){
			menuToggled = false;
			nav.image = '/images/more-list-7.png';
			menuView.animate(aDown);
		}
	});
    Ti.App.addEventListener('menuToggled', function(){
    	menuToggled = false;
		nav.image = '/images/more-list-7.png';
    });
    
	nav.addEventListener('click', function(e){

		if (menuToggled == false){
			nav.image = '/images/more-list-7-s.png';
			menuToggled = true;
			menuView.animate(aUp);
			//menuView.backgroundImage = 'images/transparent-bg.png';
		} else {
			menuToggled = false;
			menuView.animate(aDown);
			nav.image = '/images/more-list-7.png';
		}
	});
	
/*	
	var leafletPip = require('/lib/leaflet-pip');
	var url = "http://reception.chhai/images/LGA.geojson";
	var json;
	 
	var xhr = Ti.Network.createHTTPClient({
	    onload: function() {
			// parse the retrieved data, turning it into a JavaScript object
	    	var json = JSON.parse(this.responseText);
	    	
	    	var feature = json.features[0];
	    	//var gjLayer = L.geoJson(json);
			var results = leafletPip.pointInLayer([135.884762788,-33.850199904], feature.geometry);
			Ti.API.info(results);
	    	
	    	
	    	Ti.API.info('------process json');
	    	var feature = json.features[0];
			//Ti.API.info(feature.geometry);
			var singlepoint = {"type": "Point", "coordinates": [135.884762788,-33.850199904]};
			var multipoly = feature.geometry;

			if (gju.pointInMultiPolygon(singlepoint,multipoly)) {
				Ti.API.info('GOT POINTS-----');
			} else {
				Ti.API.info('----no point');
			}

	    	
		}
	});
	xhr.open('GET',url);
	xhr.send();
	*/
	  	/**
	 * The following snippet will ask the user to rate your app the second time they launch it.
	 * It lets the user rate it now, "Remind Me Later" or never rate the app.
	 */
	var win2 = Ti.UI.createWindow({ backgroundColor: '#fff' });
	win2.addEventListener('open', checkReminderToRate);
	//win2.add(Ti.UI.createLabel({ text: 'This is a simple app that will remind you to rate it.' }));
	win2.open();
	 
	function checkReminderToRate() {
	    var now = new Date().getTime();
	    var remindToRate = Ti.App.Properties.getString('RemindToRate');
	    if (!remindToRate) {
	        Ti.App.Properties.setString('RemindToRate', now);
	    }
	    else if (remindToRate < now) {
	        var alertDialog = Titanium.UI.createAlertDialog({
	            title: 'Please rate this app!',
	            message: 'Would you take a moment to rate this app?',
	            buttonNames: ['OK', 'Remind Me Later', 'Never'],
	            cancel: 2
	        });
	        alertDialog.addEventListener('click', function(evt) {
	            switch (evt.index) {
	                case 0:
	                    Ti.App.Properties.setString('RemindToRate', Number.MAX_VALUE);
	                    // NOTE: replace this with your own iTunes link; also, this won't WON'T WORK IN THE SIMULATOR!
	                    if (Ti.Android) {
	                        Ti.Platform.openURL('URL TO YOUR APP IN THE GOOGLE MARKETPLACE');
	                    }
	                    else {
	                        Ti.Platform.openURL('URL TO YOUR APP IN THE ITUNES STORE');
	                    }
	                    break;
	                case 1:
	                    // "Remind Me Later"? Ok, we'll remind them tomorrow when they launch the app.
	                    Ti.App.Properties.setString('RemindToRate', now + (1000 * 60 * 60 * 24));
	                    break;
	                case 2:
	                    Ti.App.Properties.setString('RemindToRate', Number.MAX_VALUE);
	                    break;
	            }
	        });
	        alertDialog.show();
	    }
	}
	  	
	
	
	
	
	return win;
}

