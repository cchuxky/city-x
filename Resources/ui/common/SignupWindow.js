exports.Signup = function(){
	var window = Ti.UI.createWindow({
		title: 'SIGNUP',
		backButtonTitle: '',
		layout: 'vertical',
		backgroundColor: Ti.App.Properties.getString('backgroundColor'),
		barColor: Ti.App.Properties.getString('barColor'),
		navTintColor: Ti.App.Properties.getString('tintColor'),
		//statusBarStyle: Ti.UI.iPhone.StatusBar.LIGHT_CONTENT,
		fullscreen: true,
		//bottom: '-1000dp',
	});

	var usernameText = Ti.UI.createTextField({
		autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_ALL,
		top: 0,
		hintText: 'USERNAME',
		maxLength: 15,
		width: '100%',
		height: '60dp',
		backgroundColor: '#fff',
	    style: 'none',
	    textAlign: 'center',
	    font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
	    paddingLeft: 10,
	    borderColor: '#D4DEE5',
	    keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
	    returnKeyType: Titanium.UI.RETURNKEY_NEXT,  
	});
	window.add(usernameText);
	
	var passwordText = Ti.UI.createTextField({
		top: 0,
		hintText: 'PASSCODE',
		width: '100%',
		height: '60dp',
		textAlign: 'center',
	    font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		backgroundColor: '#fff',
		borderColor: '#D4DEE5',
	    style: 'none',
	    paddingLeft: 10,
	    keyboardType: Titanium.UI.KEYBOARD_NUMBER_PAD,
	    returnKeyType: Titanium.UI.RETURNKEY_GO,
	    maxLength: 4,
	});
	window.add(passwordText);
	
	
	var style = Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN;
	if (Titanium.Platform.name == 'iPhone OS'){
		style = Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN;
	}
	var status = Ti.UI.createActivityIndicator({
		style: style,
		left: 50,
	});
	status.hide();
	
	
	usernameText.addEventListener('return', function(){
		passwordText.focus();
	});
	

	
	var signupBtn = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: 'SIGN UP',
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: Titanium.App.Properties.getString('buttonBg1'),
	});
	
	window.add(signupBtn);
	
	var backBtn = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: 'BACK',
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: Titanium.App.Properties.getString('buttonBgBack'),
	});
	
	window.add(backBtn);
	
	backBtn.addEventListener('click', function(){
		window.close();
	});
	
	passwordText.addEventListener('return', join);
	signupBtn.addEventListener('click', join);
	
	function join(){
		
		if(usernameText.value == '' || passwordText.value == '' || usernameText.value.length < 4){
			shake(usernameText);
			shake(passwordText);
			return;
		}
		
		signupBtn.add(status);
		status.show();
		
		Cloud.Users.create({
		    username: usernameText.value,
		    password: passwordText.value,
		    password_confirmation: passwordText.value,
		    //custom_fields: {lat: '', lon: ''},
		}, function (e) {
		    if (e.success) {
		        var user = e.users[0];
		     	            
		        // login    
		        Cloud.Users.login({
				    login: usernameText.value,
				    password: passwordText.value,
				}, function (ev) {
				    if (ev.success) {
				        var user = ev.users[0];
				        Ti.App.Properties.setString('sessionId', ev.meta.session_id);
				        Ti.App.Properties.setObject('user', user);
				        Ti.App.fireEvent('loggedIn');
				        
				        Cloud.PushNotifications.subscribe({
					        channel: 'alert',
					        device_token: Ti.App.Properties.getString('deviceToken'),
					        type: Ti.Platform.name == 'android' ? 'android' : 'ios'
					    }, function (evt) {
					        if (evt.success) {
					           // alert('Subscribed');
					        } else {
					        	
					            /*alert('Error:\n' +
					                ((evt.error && evt.message) || JSON.stringify(evt)));*/
					        }
					    });
					    window.close();
					   
				    } else {
				    	
				       // alert(((ev.error && ev.message) || JSON.stringify(ev)));
				    }
				});    
		            
		    } else {
		    	shake(usernameText);
		    	usernameText.value = '';	
	        	usernameText.hintText = 'USERNAME TAKEN';
	        	setTimeout(function(){
					usernameText.hintText = 'USERNAME';	
					usernameText.value = '';						    	
			    },2000);
			    Ti.API.info(e.message);
			    Ti.API.info(e.error);
		        //alert(((e.error && e.message) || JSON.stringify(e)));
		    }
		    status.hide();
		});
	};
	
	window.addEventListener('focus', function(){
		usernameText.focus();
	});
	
	return window;
};


