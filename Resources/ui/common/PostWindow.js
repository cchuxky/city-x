function Post(media) {
	// main camera window
	var win = Titanium.UI.createWindow({
		barColor: '#000',
		closeOnExit: true,
		navBarHidden: true,
		backgroundColor: Ti.App.Properties.getString('backgroundColor'),
		fullscreen: true,
	});
	
	
	
		// get current address
	if (Ti.Geolocation.locationServicesEnabled) {
			Ti.Geolocation.purpose = 'Your current location will be registered when reporting an issue';
			Ti.Geolocation.accuracy = Titanium.Geolocation.ACCURACY_NEAREST_TEN_METERS;
			Ti.Geolocation.getCurrentPosition(handleLocation);
	}
	
	var qlat = '';
	var qlon = '';
	var addr;
	
	Ti.App.addEventListener('gotPlace',function(e){	
		var addr = e.address;
		var addrArr = addr.split(',');
		var suburb = addrArr[2];
		addr = 'Location: '+suburb+', '+e.postcode;
		qlat = e.lat;
		qlon = e.lon;
	});
	
	var navView = Ti.UI.createView({
		top: 0,
		height: '60dp',
	});
	
	var doneBtn = Ti.UI.createButton({
		width: '50%',
		height: '60dp',
		title: 'SUBMIT',
		right: 0,
		font: {fontSize: '20dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: '#a8dba8',
	});
	
	
	
	var cancelBtn = Ti.UI.createButton({
		width: '50%',
		height: '60dp',
		title: 'CANCEL',
		left: 0,
		font: {fontSize: '20dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: '#79bd9a',
	});
	navView.add(cancelBtn);
	navView.add(doneBtn);
	win.add(navView);
	
	
	cancelBtn.addEventListener('click', function(){
		win.close();
	});

	var thumbView = Ti.UI.createImageView({
		width: '100%',
		image: media,
		top: '60dp',
		//height: '150dp'
	});
	
	win.add(thumbView);
	
	
	var opacLayer = Ti.UI.createView({
		top: '60dp',
		width: '100%',
		height: '100%',
		backgroundColor: '#000000',
		opacity: 0.8,
	});
	win.add(opacLayer);
	
	var descText = Ti.UI.createTextArea({
		top: '60dp',
		width: '95%',
		height: '100dp',
	    style: 'none',
	    font: {fontSize: '20dp'},
	    paddingLeft: 10,
	    keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
	    returnKeyType: Titanium.UI.RETURNKEY_NEXT,  
	    maxLength: 250,
	    backgroundColor: 'transparent',
	    color: '#ffffff',
	});
	win.add(descText);
	
	
	
	var activityDesc = Ti.UI.createLabel({
		text: 'Write a suggestion or feedback...',
		font: {fontSize: '20dp'},
		color: '#ccc',
		left: 20,
		top: '70dp',
	});
	win.add(activityDesc);
	
	descText.addEventListener('change', function(){
		if (descText.value.length > 0){
			activityDesc.hide();
		} else {
			activityDesc.show();
		}
	});
	

	/* this is needed to make sure the camera is not stretched */

	var leftPos = 0;
	var imageId = 1;
	var imageFiles = [];

	win.addEventListener('focus', function(){
		descText.focus();
	});
	
	var activityIndicator = Titanium.UI.createActivityIndicator({
		message: 'Uploading image ...',
		width: '100%',
		backgroundColor:'#77AD85',
		opacity: 0.8,
		color: '#ffffff',
	});
	activityIndicator.hide();
	win.add(activityIndicator);
	
	doneBtn.addEventListener('click', function(){
		Ti.Media.hideCamera();
		//win.close();

		activityIndicator.show();
		
		var coords = Ti.App.Properties.getObject('coords');
		
		newImage = convert.resizeKeepAspectRatioNewWidth(media, 1024);
		newBlob = ImageFactory.compress(newImage, 0.8);
		
		Cloud.Places.create({
		    name: descText.value,
		    latitude: coords.lat,
		    longitude: coords.lon,
		    photo: newBlob,
		}, function (e) {
		    if (e.success) {
		        var place = e.places[0];
		        
		        Ti.App.fireEvent('newPlace', {place: place});
		        win.close();
		       /* alert('Success:\n' +
		            'id: ' + place.id + '\n' +
		            'name: ' + place.name + '\n' +
		            'updated_at: ' + place.updated_at);
		            */
		            
		    } else {
		    	alert('There is an error. Please try again.');
		    	
		        //alert('Error:\n' +
		         //   ((e.error && e.message) || JSON.stringify(e)));
		    }
		   
		});

	});
	
	return win;
}


function handleLocation(e) {
	if (e.error) {
	    alert('You need to enable geolocation for submit a report');
	}
	Ti.App.Properties.setObject('coords', {lat: e.coords.latitude, lon: e.coords.longitude});
	/*
	var fileName = 'LGA.geojson'; 
	var file = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory, fileName);    
	//var json, object_name, locatie, i, row, title, value, homeof, logo, colors, link; 
	var preParseData = (file.read().text); 
	var response = JSON.parse(preParseData); 
	    Ti.API.info(response);
	

	Titanium.Geolocation.reverseGeocoder(e.coords.latitude, 
	                                        e.coords.longitude, 
	                                        function(evt) {
	
	    var places = evt.places;
	    
	    if (places && places.length) {				    
		    Ti.App.fireEvent('gotPlace',{
		    	'city': places[0].city,
				'street': places[0].street,
				'street1': places[0].street1,
				'postcode': places[0].zipcode, 
				'address': places[0].address,
				'country': places[0].country,
				'region1': places[0].region1,
				'lat': places[0].latitude,
				'lon': places[0].longitude,
			});
		}
	});
	*/
};
exports.Post = Post;