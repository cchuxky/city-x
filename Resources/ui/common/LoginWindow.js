exports.Login = function(){

	var window = Ti.UI.createWindow({
		title: 'LOG IN',
		layout: 'vertical',
		backgroundColor: Ti.App.Properties.getString('backgroundColor'),
		//bottom: Ti.Platform.displayCaps.platformHeight,
		statusBarStyle: Ti.UI.iPhone.StatusBar.TRANSLUCENT_BLACK,
		fullscreen: true,
		//bottom: '-1000dp',
	});
	
	
//	var Cloud = require('ti.cloud');
//	Cloud.debug = true;

	
	var usernameText = Ti.UI.createTextField({
		autocapitalization: Titanium.UI.TEXT_AUTOCAPITALIZATION_ALL,
		hintText: 'USERNAME',
		width: '100%',
		height: '60dp',
		backgroundColor: '#fff',
	    style: 'none',
	    textAlign: 'center',
	    font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
	    paddingLeft: 10,
	    borderColor: '#D4DEE5',
	    keyboardType: Titanium.UI.KEYBOARD_DEFAULT,
	    returnKeyType: Titanium.UI.RETURNKEY_NEXT,  
	    maxLength: 15,
	});
	window.add(usernameText);
	
	var passwordText = Ti.UI.createTextField({
		hintText: 'PASSCODE',
		width: '100%',
		height: '60dp',
		textAlign: 'center',
		passwordMask: true,
	    font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		backgroundColor: '#fff',
		borderColor: '#D4DEE5',
	    style: 'none',
	    paddingLeft: 10,
	    keyboardType: Titanium.UI.KEYBOARD_NUMBER_PAD,
	    returnKeyType: Titanium.UI.RETURNKEY_GO,
	    maxLength: 4,
	});
	window.add(passwordText);
	
	usernameText.addEventListener('return', function(){
		passwordText.focus();
	});
	
	var loginBtn = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: 'LOG IN',
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: Titanium.App.Properties.getString('buttonBg1'),
	});
	
	window.add(loginBtn);
	var backBtn = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: 'BACK',
		font: {fontSize: '25dp', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: Titanium.App.Properties.getString('buttonBgBack'),
	});
	
	window.add(backBtn);
	
	backBtn.addEventListener('click', function(){
		window.close();
	});
	
	var style = Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN;
	if (Titanium.Platform.name == 'iPhone OS'){
		style = Ti.UI.iPhone.ActivityIndicatorStyle.PLAIN;
	}
	
	var status = Ti.UI.createActivityIndicator({
		style: style,
		left: 50,
	});
	loginBtn.add(status);
		
	passwordText.addEventListener('return', login);
	
	loginBtn.addEventListener('click', login);
	
	function login(){
		
		if(usernameText.value == '' || passwordText.value == ''){
			shake(usernameText);
			shake(passwordText);
			return;
		}
		
		status.show();
		Cloud.Users.login({
		    login: usernameText.value,
		    password: passwordText.value,
		}, function (e) {
		    if (e.success) {
		        var user = e.users[0];
		        Ti.App.Properties.setString('sessionId', e.meta.session_id);
		        Ti.App.Properties.setObject('user', user);
		        Ti.App.fireEvent('loggedIn');
		        
		         Cloud.PushNotifications.subscribe({
			        channel: 'alert',
			        device_token: Ti.App.Properties.getString('deviceToken'),
			        type: Ti.Platform.name == 'android' ? 'android' : 'ios'
			    }, function (e) {
			        if (e.success) {
			           // alert('Subscribed');
			        } else {
			            alert('Error:\n' +
			                ((e.error && e.message) || JSON.stringify(e)));
			        }
			    });
			    		        
		    } else {
		    	shake(usernameText);
				shake(passwordText);
		        //alert(((e.error && e.message) || JSON.stringify(e)));
		    }
		    status.hide();
		});
	};
	
	
	
	
	
	window.addEventListener('focus', function(){
		usernameText.focus();
	});
	return window;
};


