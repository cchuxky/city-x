//Application Window Component Constructor
function ApplicationWindow() {
	//create component instance
	var self = Ti.UI.createWindow({
		backgroundColor:'#000000',
		//layout: 'vertical',
	});


	var logoImage = Titanium.UI.createImageView({
		image: '/images/city.png',
		top: '70dp',
		width: '80dp',
		
	});
	
	var logo = Ti.UI.createLabel({
		font: {fontSize: '45dp', fontWeight: 'bold', fontFamily: 'NexaBlack'},
		text: 'CITY X',
		color: '#fff',
		top: '150dp',
	})
	self.add(logo);
	//self.add(logoImage);
	


	var loginBtn = Ti.UI.createButton({
		backgroundColor: Titanium.App.Properties.getString('buttonBg1'), 
		width: '100%',
		height: '60dp',
		title: 'LOG IN',
		font: {fontSize: '25dp', fontWeight: 'bold', fontFamily: 'NexaBlack'},
		style: 'none',
		bottom: '60dp',
		color:Titanium.App.Properties.getString('buttonColor'),
	});
	
	self.add(loginBtn);
	
	var signupBtn = Ti.UI.createButton({
		width: '100%',
		height: '60dp',
		title: 'SIGN UP',
		font: {fontSize: '25dp', fontWeight: 'bold', fontFamily: 'NexaBlack'},
		style: 'none',
		color:Titanium.App.Properties.getString('buttonColor'),
		backgroundColor: Titanium.App.Properties.getString('buttonBg2'),
		bottom: 0,
	});
	
	self.add(signupBtn);
	
	
	var aUp = Ti.UI.createAnimation({
		bottom: 0,
		duration: 500,
	});
	
	var aDown = Ti.UI.createAnimation({
		bottom: '-600dp',
		top: 0,
	});
	
	loginBtn.addEventListener('click', function(){
		var Login = require('ui/common/LoginWindow').Login;
		var loginWindow = new Login();
		loginWindow.open();
		//loginWindow.animate(aUp);
		//self.openWindow(loginWindow, {animated: true});
	});
	
	signupBtn.addEventListener('click', function(){
		var Signup = require('ui/common/SignupWindow').Signup;
		var signupWindow = new Signup();
		signupWindow.open();
		//signupWindow.animate(aUp);
		//self.openWindow(signupWindow, {animated: true});
	});


	return self;
}

//make constructor function the public component interface
module.exports = ApplicationWindow;
