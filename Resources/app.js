

//bootstrap and check dependencies
if (Ti.version < 1.8) {
  alert('Sorry - this application template requires Titanium Mobile SDK 1.8 or later');
}

// This is a single context application with multiple windows in a stack
(function() {
  //render appropriate components based on the platform and form factor
  var osname = Ti.Platform.osname,
    version = Ti.Platform.version,
    height = Ti.Platform.displayCaps.platformHeight,
    width = Ti.Platform.displayCaps.platformWidth;

  //considering tablets to have width over 720px and height over 600px - you can define your own
  function checkTablet() {
    var platform = Ti.Platform.osname;

    switch (platform) {
      case 'ipad':
        return true;
      case 'android':
        var psc = Ti.Platform.Android.physicalSizeCategory;
        var tiAndroid = Ti.Platform.Android;
        return psc === tiAndroid.PHYSICAL_SIZE_CATEGORY_LARGE || psc === tiAndroid.PHYSICAL_SIZE_CATEGORY_XLARGE;
      default:
        return Math.min(
          Ti.Platform.displayCaps.platformHeight,
          Ti.Platform.displayCaps.platformWidth
        ) >= 400
    }
  }

  Ti.include('/lib/functions.js');
  ImageFactory = require('ti.imagefactory');
  convert = require('lib/convert');
	
  var isTablet = checkTablet();
  console.log(isTablet);
  
  
  Titanium.App.Properties.setString('barColor', '#fff');
	Titanium.App.Properties.setString('barColor2', '#15c8fb');
	Titanium.App.Properties.setString('tintColor', '#007aff');
	Titanium.App.Properties.setString('backgroundColor', '#5A8600');
	Titanium.App.Properties.setString('buttonColor', '#ffffff');
	Titanium.App.Properties.setString('buttonTextColor', '#003C5B');
	Titanium.App.Properties.setString('buttonBg1', '#409ca9'),
	Titanium.App.Properties.setString('buttonBg2', '#4ab2c0'),
	Titanium.App.Properties.setString('buttonBg3', '#51c4d4'),
	Titanium.App.Properties.setString('buttonBg4', '#5ad9eb'),
	//Titanium.App.Properties.setString('buttonBg', '#4ab2c0'),
	Titanium.App.Properties.setString('buttonBgBack', '#4ab2c0'),
	Titanium.App.Properties.setString('alertBg', '#FF0000'),

  Cloud = require('ti.cloud');
  Cloud.debug = true; 
  
  
  if (Titanium.Platform.name == 'iPhone OS'){
	Social = require('dk.napp.social');
  }
  var session = Ti.App.Properties.getObject('user');
  moment = require('lib/moment-timezone');
  
	if ( Ti.App.Properties.getString('sessionId')){
		Ti.API.info('inside got seesion id');
		Cloud.sessionId =  Ti.App.Properties.getString('sessionId');
		
		Cloud.Users.showMe(function (e) {
	    if (e.success) {
		        var user = e.users[0];
		        Ti.App.Properties.setObject('user', user);
		        Ti.App.fireEvent('loggedIn');
		    }
		});
	} else {
    	var Window;
	/*  if (isTablet) {
	    Window = require('ui/tablet/ApplicationWindow');
	  } else {
	*/  	
	    // Android uses platform-specific properties to create windows.
	    // All other platforms follow a similar UI pattern.
	    if (osname === 'android') {
	      Window = require('ui/handheld/android/ApplicationWindow');
	    } else {
	      Window = require('ui/handheld/ApplicationWindow');
	    }
	//  }
	  	new Window().open();
  
  	}
  	
  	Ti.App.addEventListener('loggedIn', function(e){
		if (Ti.Geolocation.locationServicesEnabled) {
			Titanium.Geolocation.purpose = 'For recording the location of faults.';
			Titanium.Geolocation.ACCURACY_NEAREST_TEN_METERS;
			//Titanium.Geolocation.distanceFilter = 10;
			//Titanium.Geolocation.trackSignificantLocationChange = true;
		} else {
			alert('Please enable location service');
		}
		
		var Main = require('ui/common/MainWindow').Main;
		var mainWindow = new Main();
		mainWindow.open();
		
		// get appstore urls
		
		if (Ti.Android) {
			var appstoreUrl = 'androidurl';
		} else {
			var appstoreUrl = 'isourl';
		}
		Cloud.KeyValues.get({
		    name: appstoreUrl
		}, function (e) {
		    if (e.success) {
		        var keyvalue = e.keyvalues[0];
		        var value = keyvalue.value;
		        Ti.App.Properties.setList('appstoreurl', value);
		    }
		});

		
		if(Ti.Geolocation.AUTHORIZATION_AUTHORIZED){		
			Titanium.Geolocation.getCurrentPosition(function(ev){
				Ti.App.Properties.setObject('coords', {lat: ev.coords.latitude, lon: ev.coords.longitude});
			});
		} else {
			alert("Please allow icy to use location service");
		}
	});
  
  	Ti.App.addEventListener('logout', function(){
		Cloud.Users.logout(function (e) {
		    if (e.success) {
		    	Ti.App.Properties.setString('sessionId', '');
		    	
		    	
		    	
		    } else {
		        alert('Error:\n' +
		            ((e.error && e.message) || JSON.stringify(e)));
		    }
		});
		
		var Window;
		    if (osname === 'android') {
	      Window = require('ui/handheld/android/ApplicationWindow');
	    } else {
	      Window = require('ui/handheld/ApplicationWindow');
	    }
	  	new Window().open();
	});

  
})();
