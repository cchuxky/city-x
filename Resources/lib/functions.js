function obj_to_query(obj) {
    var parts = [];
    for (var key in obj) {
        if (obj.hasOwnProperty(key)) {
            parts.push(encodeURIComponent(key) + '=' + encodeURIComponent(obj[key]));
        }
    }
    return "?" + parts.join('&');
}

function shake(view) {
	    
	    var tr_init = Titanium.UI.create2DMatrix();
	    var tr_start = tr_init.translate(7,0);
	    var tr_anim = tr_init.translate(-14,0);
	    
	    //Translation to start from
	    view.transform=tr_start;
	   
	    //Animation
	    var a = Titanium.UI.createAnimation();
	    a.transform = tr_anim;
	    a.duration = 50;
	    a.autoreverse = true;
	    a.repeat = 3;
	    a.delay = 0;
	
	    //Return to initial position
	    a.addEventListener('complete',function() {    
	      view.transform=tr_init;
	    });
	
	    //Execute Animation
	    view.animate(a);
}//shake

// search matching array
function isBlockedByMe(objName, value) {  
    var index = false;
    //var objName = Ti.App.Properties.getObject('block');
    if (objName){
	    for (var i = 0; i < objName.length; i++) {
	       var obj = objName[i];
	       if (obj.blocked_id == value){
	        	index = true;
	       }
	    }
    }
    return index;
} 

// search matching array
function isBlockedMe(objName, value) {  
    var index = false;
    //var objName = Ti.App.Properties.getObject('blockedMe');
    for (var i = 0; i < objName.length; i++) {
       var obj = objName[i];
       if (obj.owner_id == value){
        	index = true;
       }
    }
    Ti.API.info('blocked me'+index);
    return index;
} 

function googleReverseGeo(lat, lon, callback){
	// GOOGLE REVERSE GEOCODE, DISABLE THIS FOR NOW
	var treq = Titanium.Network.createHTTPClient();  
	var url = "http://maps.googleapis.com/maps/api/geocode/json?latlng="+lat+","+lon+"&sensor=false";
	treq.open("GET",url);
	treq.send();
	treq.onload = function(){
		Ti.API.info(this.responseText);
		var data = JSON.parse(this.responseText);
		//Ti.API.info('location loaded');
		//Ti.API.info(data.results[0].address_components);
		
		var result = data.results[0];
		Ti.API.info('----google map resutl');
		Ti.API.info(result);
		if (typeof result == 'object'){
			var placeData = result.address_components[2];
			var components = result.address_components;
			var city;
			for (var i=0; i<components.length; i++){
				var addr = components[i];
				var addr_types = addr.types.toString();
				var types = addr_types.split(',');
				for (var j=0; j<types.length; j++){
					var type = types[j];
					if (type == 'locality'){
						city = addr.long_name;	
					}
				}
			}
		}
		if (callback){
			callback(city);
		}
	};
}


function calculate_distant(lat1, lon1, lat2, lon2){
	var R = 6371; // km
	var dLat = toRad(lat2-lat1);
	var dLon = toRad(lon2-lon1);
	var lat1 = toRad(lat1);
	var lat2 = toRad(lat2);
	
	var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
	        Math.sin(dLon/2) * Math.sin(dLon/2) * Math.cos(lat1) * Math.cos(lat2); 
	var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 
	var d = R * c;
	
	return d.toFixed(1);
}

function uc_first(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}


function toRad(Value) {
    /** Converts numeric degrees to radians */
    return Value * Math.PI / 180;
}

