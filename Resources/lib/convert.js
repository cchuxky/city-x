function getNewHeight(image, newWidth) {
	var imageWidth = image.width;
    var imageHeight = image.height;
    
    if (imageWidth <= 0 || imageHeight <= 0 || newWidth <= 0)
        return blob;

    var ratio = imageWidth /imageHeight;

    var h = newWidth / ratio;
   // Ti.API.info('width: '+w);
    		
	return h; 
}  


//return new width
function getNewWidth(image, newHeight) {
	var imageWidth = image.width;
    var imageHeight = image.height;
    
    if (imageWidth <= 0 || imageHeight <= 0 || newHeight <= 0)
        return blob;

    var ratio = imageHeight / imageWidth;

    var w = newHeight / ratio;
   // Ti.API.info('width: '+w);
    		
	return w; 
}   


function resizeKeepAspectRatioNewWidth(blob, newWidth) {
	    // only run this function if suitable values have been entered
    var imageWidth = blob.width;
    var imageHeight = blob.height;
    
    if (imageWidth <= 0 || imageHeight <= 0 || newWidth <= 0)
        return blob;

    var ratio = imageWidth / imageHeight;

    var w = newWidth;
    var h = newWidth / ratio;

	return blob.imageAsResized(w, h);
}
 



function urldecode (str) {
  return encodeURI(decodeURIComponent((str + '').replace(/\+/g, '%20')));
}	

var Utils = {
  /* modified version of https://gist.github.com/1243697 */
  _getExtension: function(fn) {
    // from http://stackoverflow.com/a/680982/292947
    var re = /(?:\.([^.]+))?$/;
    var tmpext = re.exec(fn)[1];
    return (tmpext) ? tmpext : '';
  },
  RemoteImage: function(a){
    a = a || {};
    var md5;
    var needsToSave = false;
    var savedFile;
    if(a.image){
      md5 = Ti.Utils.md5HexDigest(a.image)+this._getExtension(a.image);
      savedFile = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,md5);
      if(savedFile.exists()){
        a.image = savedFile;
      } else {
        needsToSave = true;
      }
    }
    var image = Ti.UI.createImageView(a);
    if(needsToSave === true){
      function saveImage(e){
        image.removeEventListener('load',saveImage);
        savedFile.write(
          Ti.UI.createImageView({image:image.image,width:'auto',height:'auto', bubbleParent: true}).toImage()
        );
      }
      image.addEventListener('load',saveImage);
    }
    return image;
  }
};


var getRemoteFile = function(file, remote, callback) {
 	var f = Titanium.Filesystem.getFile(Titanium.Filesystem.applicationDataDirectory,file);
	if (!f.exists()) {
    	var c = Titanium.Network.createHTTPClient();
    c.onload = function() {
        f.write(this.responseData);
        if (callback){
		callback(f.nativePath);
	}
    }
    c.open('GET',remote, true);
        c.send();         
    }
   
   
}

exports.getRemoteFile = getRemoteFile;
exports.Utils = Utils;
exports.getNewHeight = getNewHeight;
exports.getNewWidth = getNewWidth;
exports.urldecode = urldecode;
exports.resizeKeepAspectRatioNewWidth = resizeKeepAspectRatioNewWidth;
