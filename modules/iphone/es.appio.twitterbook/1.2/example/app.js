// Create a Window with 4 buttons to:
// - Check the availability of Facebook / Twitter
// - Post on Facebook
// - Post on Twitter
// - Post on Sina Weibo

// Require the module and check if it was successfully loaded
var twitterbook = require('es.appio.twitterbook');
Ti.API.info("Module Loaded => " + twitterbook);

var win = Ti.UI.createWindow({
	backgroundImage: 'bgn_twitterbook.png',
	layout : 'vertical'
});

// isFacebookAvailable / isTwitterAvailable / isSinaWeiboAvailable functions tell us whether they are available or not. 
// The result is a STRING with 3 possible status:
// - "available": everything is OK, you can post
// - "notLogged": the device has the capacity to post, but the user is not logged. DON'T WORRY if you try to post in this
// case, a popup will alert the user to log in.
// - "notAvailable": the device has not this capacity, because of the iOS version. For instance, this would be the result 
// of isFacebookAvailable on iOS 5. 

// Tip: if you try to post and the availability is "notAvailable" you will receive an error telling you that is not available, 
// so you can check that error instead of using this avaibality methods.
var btnAvailable = Ti.UI.createButton({
	title : 'Check Availability',
	backgroundImage: 'btn.png',
	top: "42%",
	height: 54,
	width: 245
});
btnAvailable.addEventListener('click', function() {

	alert("Facebook availability: " + twitterbook.isFacebookAvailable());
	alert("Twitter availability: " 	+ twitterbook.isTwitterAvailable());
	alert("Sina Weibo availability: " 	+ twitterbook.isSinaWeiboAvailable());
	
});
win.add(btnAvailable);


// Post on Facebook
var btnFacebook = Ti.UI.createButton({
	title : 'Facebook',
	backgroundImage: 'btn.png',
	top: "3%",
	height: 54,
	width: 245
});
// We post a message, 2 URL and 3 photos (facebook allows to post more than 1 photo). The third photo is a BLOB. Yes, you can post both BLOBs and file images
// We pass it the callbacks for success, cancel and error
btnFacebook.addEventListener('click', function() {
	
	var imageBlobWin = win.toImage();
	
	twitterbook.post({
		type : 'facebook', //(required)
		message : 'Appio Rocks ;)', // (optional)
		urls : ['http://www.appio.es', 'https://twitter.com/appio_spain'],// (optional) Note: This is ALWAYS an ARRAY
		images : ['logo_twitterbook.png', 'logo_appio.png', imageBlobWin],// (optional) Note: This is ALWAYS an ARRAY
		success : function() {
			alert("Post successfully");
		},
		cancel : function() {
			alert("The user has canceled the post");
		},
		error : function(e) {
			alert("There is an error: ");
			alert(e);
		}
	});
});
win.add(btnFacebook);


// Twitter
var btnTwitter = Ti.UI.createButton({
	title : 'Twitter',
	backgroundImage: 'btn.png',
	top: "3%",
	height: 54,
	width: 245
});
// We post a message, 2 URL and 1 photo (Twitter allows to post just 1 photo). In this case the image is a file (a path to a file) but it could be also a BLOB 
// We pass it the callbacks for success, cancel and error
btnTwitter.addEventListener('click', function() {
	twitterbook.post({
		type : 'twitter', //(required)
		message : 'TwitterBook Module by @appio_spain', //(optional) 
		urls : ['http://www.appio.es', 'https://twitter.com/appio_spain'],// (optional) Note: This is ALWAYS an ARRAY
		images : ['logo_appio.png'],// (optional) Note: This is ALWAYS an ARRAY
		success : function() {
			alert("Post successfully");
		},
		cancel : function() {
			alert("The user has canceled the post");
		},
		error : function(e) {
			alert("There is an error: ");
			alert(e);
		}
	});
});
win.add(btnTwitter);


// Sina Weibo
var btnSina = Ti.UI.createButton({
	title : 'Sina Weibo',
	backgroundImage: 'btn.png',
	top: "3%",
	height: 54,
	width: 245
});
// We post a message, 2 URL and 1 photo (Sina Weibo allows to post just 1 photo). In this case the image is a file (a path to a file) but it could be also a BLOB 
// We pass it the callbacks for success, cancel and error
btnSina.addEventListener('click', function() {
	
	var imageBlobWin = win.toImage();
	
	twitterbook.post({
		type : 'sinaweibo', // (required)
		message : 'Made by Appio', // (optional) 
		urls : ['http://www.appio.es', 'https://twitter.com/appio_spain'],// (optional) Note: This is ALWAYS an ARRAY
		images : ['logo_appio.png'], // (optional) Note: This is ALWAYS an ARRAY
		success : function() {
			alert("Post successfully");
		},
		cancel : function() {
			alert("The user has canceled the post");
		},
		error : function(e) {
			alert("There is an error: ");
			alert(e);
		}
	});
});
win.add(btnSina);

win.open();

